@extends('layout.main')
{{-- section ('('nama yield', 'valuenya')') --}}
@section('menu-title', 'Beli Tiket')
@section('menu-bootcamp', 'active')
@section('side-title', 'Beli')
@section('content')

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div>
        <form id="editForm" action="{{ url('/beli/') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <input name="userId" type="hidden" class="form-control" id="exampleInputEmail1">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Nama</label>
                    <input name="tiketName" type="text" class="form-control" id="exampleInputEmail1">
                </div>
                {{-- <div class="form-group">
                    <label for="exampleInputEmail1">No Tiket</label>
                    <input name="tiketNoTiket" type="text" class="form-control" id="exampleInputEmail1">
                </div> --}}
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input name="tiketEmail" type="text" class="form-control" id="exampleInputEmail1" >
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">No Telpon</label>
                    <input name="tiketNoTelp" type="text" class="form-control" id="exampleInputEmail1">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Alamat</label>
                    <input name="tiketAddress" type="text" class="form-control" id="exampleInputEmail1">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Tanggal</label>
                    <input name="tiketDate" type="datetime-local" class="form-control" id="exampleInputEmail1" >
                </div>

                
                <div class="card">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                
                {{-- @if($action!='show')
                    <div class="card">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                @endif --}}
            </div>
        </form>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <script>
        // Tambahkan event listener untuk form submit
        document.getElementById('editForm').addEventListener('submit', function(event) {
            event.preventDefault(); // Mencegah form submit
    
            // Lakukan AJAX request untuk mengirim data form
            fetch(this.action, {
                method: this.method,
                body: new FormData(this)
            })
            .then(response => {
                if (response.ok) {
                    // Jika respons berhasil, tampilkan SweetAlert dengan pesan sukses
                    Swal.fire({
                        icon: 'success',
                        title: 'Success!',
                        text: 'Data has been added successfully.',
                    }).then(() => {
                        // Redirect pengguna ke halaman tertentu setelah menutup SweetAlert
                        window.location.href = '/tiket';
                    });
                    // Reset form
                    this.reset();
                } else {
                    // Jika respons tidak berhasil, tampilkan SweetAlert dengan pesan error
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong!',
                    });
                }
            })
            .catch(error => {
                console.error('Error:', error);
                // Tampilkan SweetAlert dengan pesan error
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                });
            });
        });
    </script>
</body>
</html>
@endsection