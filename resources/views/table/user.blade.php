@extends('layout.main')
{{-- section ('('nama yield', 'valuenya')') --}}
@section('menu-title', 'Master Admin')
@section('menu-user', 'active')
@section('side-title', 'User')
@section('content')

    <div>
        <div>
            <a href="/register" class="btn btn-primary">Tambah</a>
        </div>

        <div id="tabel">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Master Admin</h3>
                </div>
        
                <div class="card-body">
                    <table id="myTable" class="table table-hover text-nowrap" 
                     {{-- data-page-length='3' --}}
                     >
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $index => $row)
                                <tr>
                                    
                                    <td>{{ $index + 1 }}</td>
                                    <td>{{ $row['name'] }}</td>
                                    <td>{{ $row['email'] }}</td>
                                    <td>
                                        <a href="{{ url('/user-edit/'.$row->id) }}" class="btn btn-outline-success">Edit</a>
                                        {{-- <a href="{{ url('/user-delete/'.$row->id) }}" class="btn btn-outline-danger">Delete</a> --}}
                                        <button class="btn btn-outline-danger delete-btn" data-id="{{ $row->id }}">Delete</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('custom-script')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            let table = new DataTable('#myTable', {
                paging: true
            });

            // Tangani klik tombol hapus
            document.querySelectorAll('.delete-btn').forEach(button => {
                button.addEventListener('click', function () {
                    let userId = this.getAttribute('data-id');

                    Swal.fire({
                        title: 'Are you sure??',
                        text: "You won't be able to return this!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, just delete it!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            // Redirect untuk menghapus data
                            window.location.href = "{{ url('/user-delete/') }}" + '/' + userId;
                        }
                    });
                });
            });
        });
    </script>
@endsection

{{-- @section('custom-script')
    <script>
        let table = new DataTable('#myTable', {
            paging: true
    });
    </script>
@endsection --}}