@extends('layout.main')
{{-- section ('('nama yield', 'valuenya')') --}}
@section('menu-title', 'Detail User')
@section('menu-bootcamp', 'active')
@section('content')

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div>
        <form id="editForm" action="{{ url('/edit-user-proses/') }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                    <input name="userId" type="hidden" class="form-control" id="exampleInputEmail1" value="{{ $detail->id }}">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>
                    <input name="userName" type="text" pattern="[A-Za-z\s]+" title="Harap masukkan hanya huruf" class="form-control" id="exampleInputEmail1" value="{{ $detail->name }}">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input name="userEmail" type="text" class="form-control" id="exampleInputEmail1" value="{{ $detail->email }}" >
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Password</label>
                    <input name="userPassword" type="password" class="form-control" id="exampleInputEmail1" >
                </div>

                
                <div class="card">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                
                {{-- @if($action!='show')
                    <div class="card">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                @endif --}}
            </div>
        </form>
        @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
    </div>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <script>
        // Tambahkan event listener untuk form submit
        document.getElementById('editForm').addEventListener('submit', function(event) {
            event.preventDefault(); // Mencegah form submit
    
            // Lakukan AJAX request untuk mengirim data form
            fetch(this.action, {
                method: this.method,
                body: new FormData(this)
            })
            .then(response => {
                if (response.ok) {
                    // Jika respons berhasil, tampilkan SweetAlert dengan pesan sukses
                    Swal.fire({
                        icon: 'success',
                        title: 'Success!',
                        text: 'Data has been edited successfully.',
                    }).then(() => {
                        // Redirect pengguna ke halaman tertentu setelah menutup SweetAlert
                        window.location.href = '/user';
                    });
                    // Reset form
                    this.reset();
                } else {
                    // Jika respons tidak berhasil, tampilkan SweetAlert dengan pesan error
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong!',
                    });
                }
            })
            .catch(error => {
                console.error('Error:', error);
                // Tampilkan SweetAlert dengan pesan error
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                });
            });
        });
    </script>
</body>
</html>
@endsection