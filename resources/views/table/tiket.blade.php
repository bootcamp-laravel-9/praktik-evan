@extends('layout.main')
{{-- section ('('nama yield', 'valuenya')') --}}
@section('menu-title', 'Ticket Report')
@section('menu-reoirt', 'active')
@section('side-title', 'User')
@section('content')

    <div>
        <div id="tabel">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Tiket</h3>
                </div>
        
                <div class="card-body">
                    <table id="myTable" class="table table-hover text-nowrap" 
                     {{-- data-page-length='3' --}}
                     >
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nomor Tiket</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>No Telepon</th>
                            <th>Tanggal Tiket</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($data_list as $index => $row)
                                <tr>
                                    
                                    <td>{{ $row->id }}</td>
                                    <td>{{ $row->no_tiket }}</td>
                                    <td>{{ $row->name }}</td>
                                    <td>{{ $row->email }}</td>
                                    <td>{{ $row->no_telp }}</td>
                                    <td>{{ $row->date_ticket }}</td>
                                    {{-- <td>
                                        <a href="{{ url('/user-edit/'.$row->id) }}" class="btn btn-outline-success">Edit</a>
                                        <a href="{{ url('/member-delete/'.$row->id) }}" class="btn btn-outline-danger">Delete</a>
                                    </td> --}}
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('custom-script')
    <script>
        let table = new DataTable('#myTable', {
            paging: true
    });
    </script>
@endsection