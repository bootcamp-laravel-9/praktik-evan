<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Registration Page</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('AdminLTE/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('AdminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('AdminLTE/dist/css/adminlte.min.css')}}">
</head>
<body class="hold-transition register-page" style="background-image: url('https://images.unsplash.com/photo-1635776062360-af423602aff3?q=80&w=1932&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D')">
    <div class="register-box">
        <div class="register-logo">
            <b>Add User</b>
        </div>

        <div class="card" style="box-shadow: 10px 10px 10px rgb(0, 0, 0)">
            <div class="card-body register-card-body" style="border-radius: 50px">
                <p class="login-box-msg">Add a New User</p>

                <form id="registerForm" action="{{ url('/register/') }}" method="post" >
                    @csrf
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Name" name="name">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span ><i class="fa-regular fa-user"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="email" class="form-control" placeholder="Email" name="email">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span><i class="fa-regular fa-envelope"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" placeholder="Password" name="password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span><i class="fa-solid fa-lock"></i></span>
                            </div>
                        </div>
                    </div>
                    <div style="margin-top: 10px">
                        <button type="submit" class="btn btn-block" style="background-color: rgb(3, 3, 3); color: aliceblue; border-radius: 50px">
                            Add User
                        </button>
                    </div>
                </form>
                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <!-- /.form-box -->
        </div><!-- /.card -->
    </div>
    <!-- /.register-box -->

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <!-- jQuery -->
    <script src="{{ asset ('AdminLTE/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset ('AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('AdminLTE/dist/js/adminlte.min.js')}}"></script>
    <script src="https://kit.fontawesome.com/6f41e4a92f.js" crossorigin="anonymous"></script>
    {{-- <script>
        // Tambahkan event listener untuk form submit
        document.getElementById('registerForm').addEventListener('submit', function(event) {
            event.preventDefault(); // Mencegah form submit
    
            // Lakukan AJAX request untuk mengirim data form
            fetch(this.action, {
                method: this.method,
                body: new FormData(this)
            })
            .then(response => {
                if (response.ok) {
                    // Jika respons berhasil, tampilkan SweetAlert dengan pesan sukses
                    Swal.fire({
                        icon: 'success',
                        title: 'Success!',
                        text: 'Data has been added successfully.',
                    }).then(() => {
                        // Redirect pengguna ke halaman tertentu setelah menutup SweetAlert
                        window.location.href = '/user';
                    });
                    // Reset form
                    this.reset();
                } else {
                    // Jika respons tidak berhasil, tampilkan SweetAlert dengan pesan error
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong!',
                    });
                }
            })
            .catch(error => {
                console.error('Error:', error);
                // Tampilkan SweetAlert dengan pesan error
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                });
            });
        });
    </script> --}}
</body>
</html>
