<?php

use App\Http\Controllers\ConsumeApiController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});





//Route::get('/table', 'UserController@index');

//Route::put('/edit-user-proses', 'UserController@update');

Route::group(['middleware' => ['auth-session']], function () use ($router) {
    Route::get('/login', [UserController::class, 'login']);
    Route::post('/login', [UserController::class, 'loginPOST']);
});

Route::group(['middleware' => ['not-auth-session']], function () use ($router) {
    Route::get('/', function () {
        return view('dashboard.index');
    });
    Route::get('/user-edit/{id}', [UserController::class, 'edit']);
    Route::put('/edit-user-proses', [UserController::class, 'update']);
    Route::get('/user', [UserController::class, 'index']);
    Route::get('/tiket', [ConsumeApiController::class, 'index']);
    Route::post('/beli', [ConsumeApiController::class, 'beliPOST']);
    Route::get('/beli', [ConsumeApiController::class, 'beli']);
    Route::get('/register', [UserController::class, 'register']);
    Route::get('/logout', [UserController::class, 'logout']);
    Route::post('/register', [UserController::class, 'create']);
    Route::get('/user-delete/{id}', [UserController::class, 'destroy']);
});


