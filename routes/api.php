<?php

use App\Http\Controllers\ConsumeApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'UserController@show');
//Route::get('tiket', 'TiketHeaderController@index');
Route::post('create-tiket', 'TiketHeaderController@store');
Route::get('/tiket', [ConsumeApiController::class, 'index']);

Route::group(['middleware' => ['auth:sanctum','api-key']], function () use ($router) {

    //Route::post('create-tiket', 'TiketHeaderController@store');
    //Route::get('tiket', 'TiketController@index');
    //Route::put('update-brand', 'BrandController@update');
    //Route::delete('delete-brand', 'BrandController@delete');

    //Route::post('create-brand', 'BrandController@store');
    //Route::get('brand', 'BrandController@index');
    //Route::put('update-brand', 'BrandController@update');
    //Route::delete('delete-brand', 'BrandController@delete');
    //Route::get('product', 'ConsumeApiController@index');
    //Route::get('/show', [ConsumeApiController::class, 'index']);
});