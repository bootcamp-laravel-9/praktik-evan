<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

//PAKAI YANG DI PROJEK API, INI GA KEPAKAI
class TiketHeaderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $ruleUser = '';
        $ruleId = '';
        if(request()->method() != 'PUT'){
            $ruleBrand = 'unique:users,email';
        }

        // return [
        //     'id' => $ruleId,
        //     'ticket_header',
        //     'ticket_category',
        //     'total_ticket' => ['required', $ruleBrand],
        // ];

        return [
            'id' => $ruleId,
            'no_tiket',
            'name',
            'email' => ['required', $ruleBrand],
            'no_telp',
            'address',
            'date_ticket',
        ];
    }
}
