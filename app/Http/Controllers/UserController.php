<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\AuthRequest;
use App\Repositories\AuthRepository;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Message;

class UserController extends Controller
{
    // protected $AuthRepository;

    // public function __construct(AuthRepository $AuthRepository)
    // {
    //     $this->AuthRepository = $AuthRepository;
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //menampilkan blade user yang tidak di soft delete atau bool = 0
    public function index()
    {
        //menampilkan blade user yang tidak di soft delete atau bool = 0
        $data = User::all()->where('is_deleted', false);
        return view('table/user', [
            'data' => $data
        ]);
    }

    //menampilkan blade login
    public function login()
    {
        return view('auth.login');
    }

    //login untuk mendapatkan token dari api
    public function loginPOST(Request $request)
    {
        $client = new Client();
        try {
            $response = $client->request('POST', 'http://127.0.0.1:8001/api/login', [
                'json'=>[
                    "email"=> $request->email,
                    "password"=>$request->password,
                ]
            ]);
        } catch (ClientException $e) {
            //json didalam failed itu bentuknya error credential
            $failed = $e->getResponse()->getBody();
            $response_json = json_decode($failed);
            $errorMessage=$response_json->messgae;
            return redirect('/login')->with('failed', $errorMessage);
        }
        //jika sukses maka mendapatkan json sukses yang memiliki token
        $data = $response->getBody();
        $response_json = json_decode($data);
        $token=$response_json->data->token;
        //token yang didapat disimpan dalam session dengan nama credential
        session([
            'credential'=>$token,
        ]);
        return redirect('/');
    }

    // public function show(AuthRequest $request)
    // {
    //     //dd($request->password);
    //     try {
    //         $user = $this->AuthRepository->login($request);
    //         return $user;
    //     } catch (\Exception $e) {
    //         return response()->json([
    //             'status' => false,
    //             'message' => $e->getMessage(),
    //         ],400);
    //     }
    // }

    // public function show(Request $request)
    // {
    //     // $this->validate($request, [
    //     //     'password' => 'regex:/^.*(?=.*[a-zA-Z])(?=.*\d)(?=.*[!#$%&? "]).*$/'
    //     // ], [
    //     //     'password.regex' => 'must contain one lowercase, uppercase, and symbol'
    //     // ]);

    //     $profile = User::where('email', $request->email)->first();

    //     if (!$profile) {
    //         // return redirect()->with('error', 'Email atau password salah')->route('login');
    //         return back()->with('error', 'Email atau password salah');
    //     }

    //     $isValidPassword = Hash::check($request->password, $profile->password);

    //     if (!$isValidPassword) {
    //         // return redirect()->with('error', 'Email atau password salah')->route('login');
    //         return back()->with('error', 'Email atau password salah');
    //     }

    //     $token = $profile->createToken(config('app.name'))->plainTextToken;
    //     $request->session()->put('sessionLogin', $token);
    //     $request->session()->put('profileId', $profile->id);
    //     return redirect('/profile/' . $profile->id);
    // }
    

    public function register(Request $request)
    {
        return view('auth/register');
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'max:100', 'regex:/^[^\d\s]+$/u'],
            'email' => ['required', 'max:50', 'email', 'unique:users,email'],
            'password' => ['required'],
        ],
        [
            'name.regex' => 'cannot contain numbers or symbols',
        ]);
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request['password']),
            'is_deleted' => false,
        ]);
        return redirect()->intended('user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = User::find($id);
        return view('table/menu-edit', compact('detail'));
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        
        $updateuser = User::find($request->userId);

        //$updateuser->id = $request->userId;
        $updateuser->name= $request->userName;
        $updateuser->email= $request->userEmail;
        //jika password diisi atau tidak null maka akan menjalankan updateuser dan di hash
        if($request->userPassword!=null)
        {
            $updateuser->password = Hash::make($request->userPassword);
        }

        if($updateuser->save()){
            return redirect('/user');
        }
        else{
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $updateuser = User::find($id);

        //ini softdelete, nilai is_deleted akan berubah menjadi 1/true
        $updateuser->is_deleted = true;

        if($updateuser->save()){
            return redirect('/user');
        }
        else{
            return redirect()->back();
        }
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/login');
    }
}
