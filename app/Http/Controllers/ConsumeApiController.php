<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class ConsumeApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //mengambil token session yang key nya credential
        $token = session('credential');
        $client = new Client();
        $response = $client->request('GET', 'http://127.0.0.1:8001/api/tiket', [
            //menghit api dengan headers xapikey, accept app/json dan auth barear token
            //variabel token ini dapat dari session credential yang mengandung token
            'headers' => [
                'x-api-key' => 'Bootcamp_9',
                'Accept' => 'application/json',
                'Authorization' => 'Bearer '.$token
            ]
        ]);
        //setelah hit api dapat respons yang diambil bodynya dengan getBody
        $data = $response->getBody();
        //kemudian body di decode menjadi objek agar didapatkan value" nya
        $response_json = json_decode($data);
        //kemudian data" pada objek diambil dan disimpan di data_list
        $data_list=$response_json->data;
        //variabel data_list dikirim ke view untuk digunakan     
        return view('table.tiket', compact('data_list'));

        // $data = Member::all();
        // return view('table/index', [
        //     'data' => $data
        // ]);
    }

    public function beli()
    {
        return view('table.beli');
    }

    public function beliPOST(Request $request)
    {
        $token = session('credential');
        $client = new Client();
        $response = $client->request('POST', 'http://127.0.0.1:8001/api/create-tiket', [
            //mengirimkan data" yg diperlukan api dalam bntuk json
            'json'=>[
                "no_tiket"=> $request->tiketNoTiket,
                "name"=>$request->tiketName,
                "email"=>$request->tiketEmail,
                "no_telp"=>$request->tiketNoTelp,
                "address"=>$request->tiketAddress,
                "date_ticket"=>$request->tiketDate
            ],
            'headers' => [
                'x-api-key' => 'Bootcamp_9',
                'Accept' => 'application/json',
                'Authorization' => 'Bearer '.$token
            ]
        ]);
    
        return redirect('/tiket');
        
        //return view('table.tiket', compact('data_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
