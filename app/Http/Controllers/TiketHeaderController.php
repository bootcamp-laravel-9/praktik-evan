<?php

namespace App\Http\Controllers;

use App\Http\Requests\TiketHeaderRequest;
use App\Repositories\TiketHeaderRepository;
use Illuminate\Http\Request;

//PAKAI YANG DI PROJEK API, INI GA KEPAKAI
class TiketHeaderController extends Controller
{
    protected $TiketHeaderRepository;

    public function __construct(TiketHeaderRepository $TiketHeaderRepository)
    {
        $this->TiketHeaderRepository = $TiketHeaderRepository;
    }

    public function index(Request $request)
    {
        //search jadi 1 di index
        $data = $request->id === null ? $this->TiketHeaderRepository->all() : $this->TiketHeaderRepository->all($request->id);
        return $data;
    }

    public function store(TiketHeaderRequest $request)
    {
        try{
            $data = $this->TiketHeaderRepository->storeOrUpdate($request);
            return response()->json([
                'status' => true,
                'message' => 'created successfully', 
            ],200);
        }catch (\Exception $e){
            return response()->json([
                'status' => true,
                'message' => $e->getMessage(), 
            ],200);
        }
    }

    // public function update(TiketHeaderRequest $request)
    // {
    //     try {
    //         $data = $this->TiketHeaderRepository->storeOrUpdate($request, 'PUT');
    //         return response()->json([
    //             'status' => true,
    //             'message' => 'updated successfully', 
    //         ],200);
    //     } catch (\Exception $e) {
    //         return response()->json([
    //             'status' => true,
    //             'message' => $e->getMessage(), 
    //         ],200);
    //     }
    // }

    // public function delete(Request $request)
    // {
    //     try {
    //         $data = $this->TiketHeaderRepository->destroy($request->id);
    //         return response()->json([
    //             'status' => true,
    //             'message' => 'Delete Successfully'
    //         ], 200);
    //     } catch (\Exception $e) {
    //         return response()->json([
    //             'status' => false,
    //             'message' => $e->getMessage()
    //         ], 400);
    //     }
    // }
}
