<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

//PAKAI YANG DI PROJEK API, INI GA KEPAKAI
class TiketHeaderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'no_tiket' => $this->no_tiket,
            'name' => $this->name,
            'no_telp' => $this->no_telp,
            'address' => $this->address,
            'date_ticket' => $this->date_ticket,
        ];
    }
}
