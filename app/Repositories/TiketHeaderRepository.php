<?php

namespace App\Repositories;

use App\Http\Resources\TiketHeaderResource;
use App\Models\TiketHeader;
use App\Models\User;

//PAKAI YANG DI PROJEK API, INI GA KEPAKAI
class TiketHeaderRepository {

    protected $tiketHeader;

    public function __construct(TiketHeader $tiketHeader)
    {
        $this->tiketHeader = $tiketHeader;
    }

    public function storeOrUpdate($request, $method = null)
    {
        //jika method == 'PUT' maka akan menjalakan update, jika method = null maka akan menjalankan Create
        if($method == 'PUT' && !$this->tiketHeader->find($request->id))
        {
            throw new \Exception("Data User tidak ditemukan.", 400);
        }

        $data = $this->tiketHeader->updateOrCreate(
        [
            'id' => $request->id
        ],
        [
            'no_tiket' => $request->no_tiket,
            'name' => $request->name,
            'email' => $request->email,
            'no_telp' => $request->no_telp,
            'address' => $request->address,
            'date_ticket' => $request->date_ticket
        ]);
        return $data;
    }

    public function all($id = null)
    {
        if ($id == null){
            //get by id
            $response = TiketHeaderResource::collection($this->tiketHeader->all());
            return $response;
        }

        $tiketHeader = $this->tiketHeader->find($id);
        if(!$tiketHeader){
            throw new \Exception("Data user tidak ditemukan.", 400);
        }

        $response = new TiketHeaderResource($tiketHeader);
        return $response;
    }

    public function destroy($id)
    {
        $car = $this->tiketHeader->find($id);

        if(!$car){
            throw new \Exception("Data Brand tidak ditemukan.", 400);
        }
        $car->delete();
    }
}

