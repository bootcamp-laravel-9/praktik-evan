<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TiketDetail extends Model
{
    use HasFactory;
    protected $fillable = [
        'tiket_header_id',
        'tiket_category',
        'total_tiket',
    ];
}
