<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TiketHeader extends Model
{
    use HasFactory;

    protected $fillable = [
        'no_tiket',
        'name',
        'email',
        'no_telp',
        'address',
        'date_ticket',
    ];

    public $timestamps = false;

    // protected $hidden = [
    //     'created_at',
    //     'updated_at',
    //     'deleted_at',
    // ];

    // protected $casts = [
    //     'created_at' => 'datetime:Y-m-d H:i:s',
    //     'updated_at' => 'datetime:Y-m-d H:i:s',
    //     'deleted_at' => 'datetime:Y-m-d H:i:s',
    // ];
}
