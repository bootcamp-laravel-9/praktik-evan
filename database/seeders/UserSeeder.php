<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $userData = User::table('users')->first(); // Ganti 'nama_tabel' dengan nama tabel yang sesuai
        
        User::create([
            'name' => $userData->name,
            'email' => $userData->email,
            'password' => Hash::make($userData->password),
        ]);

        // User::create([
        //         'name' => 'Admin',
        //         'email' => 'example@gmail.com',
        //         'password' => Hash::make('Tes12345!'),
        //     ]);
    }
}
